import { Component, OnInit } from '@angular/core';
import { AppResultatService } from 'src/app/app.service';
import { ResultatAlgorithme } from 'src/app/model/resultatAlgorithme.model';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  resultats: ResultatAlgorithme[];
  resultatFormGroup: FormGroup;

  constructor(private appResultatService: AppResultatService,
    private fb: FormBuilder) { // <--- inject FormBuilder

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.resultatFormGroup = this.fb.group({
      resultatEntree: '',
      inconnue1: '',
      inconnue2: '',
      inconnue3: '',
      inconnue4: '',
      inconnue5: '',
      inconnue6: '',
      inconnue7: '',
      inconnue8: '',
      inconnue9: ''
    });

    this.resultatFormGroup.get('resultatEntree')
      .valueChanges
      .subscribe(data => console.log("res = " + data));

  }

  chercherSolution() {
    if (this.resultatFormGroup.get('resultatEntree').value) {
      this.appResultatService.getAllResultats(this.resultatFormGroup.get('resultatEntree').value).subscribe((data: ResultatAlgorithme[]) => {
        this.resultats = data;
      },
        error => console.log(`Server error:${error} - ${error.status} - Details: ${error.error}`));
    }
  }

}
