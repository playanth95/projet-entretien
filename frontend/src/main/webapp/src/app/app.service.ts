import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';



@Injectable()
export class AppResultatService {

    private _httpOptions = {
        headers: new HttpHeaders().append('Access-Control-Allow-Origin', '*')
            .append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
            .append('Access-Control-Allow-Headers', 'Content-Type')
    };

    _urlResultat: string = 'resultats';
    private _pathRootUrl: string = "http://localhost:8080/api/";

    constructor(private http: HttpClient) {

    };

    /**
    * Récupère tous les résultats de l'algorithme.
    */
    getAllResultats(resultat) {
        let url = this._pathRootUrl + this._urlResultat + "/" + resultat;
        return this.http.get(url,this._httpOptions);
    }

    /**
     * Modifie un resultat de l'algorithme.
     * @param resultat , le resultat à modifier.
     */
    ModifierResultat(resultat) {
        return this.http.put(this._pathRootUrl + this._urlResultat, resultat);
    }

    /**
     * Supprime un résultat de l'algorithme
     * @param resultat , le résultat à supprimer.
     */
    SupprimerResultat(resultat) {
        return this.http.delete(this._pathRootUrl + this._urlResultat, resultat);
    }
}