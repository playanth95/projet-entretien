package fr.entretien.anthony.algorithm.traitement;

import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import fr.entretien.anthony.algorithm.service.ResultatAlgorithmeService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe de traitement de l'algorithme.
 *
 * @author aattia
 */
@Component
public class TraitementAlgorithme {

  @Autowired
  ResultatAlgorithmeService resultatAlgorithmeService;

  /**
   * Méthode de lancement de l'algorithme
   * <p>
   * On génère un tableau de taille 9 avec en valeurs des float de 1 à 9.
   * A chaque itération, on re initialise ce tableau avec 9 valeurs trièes aléatoirement.
   * Les valeurs contenues dans le tableau sont les inconnues de l'équation de l'algorithme.
   * Ex(tableau en entrée : [1][2][3][4][5][6][7][8][9], on remplace chaque inconnue
   * de l'équation de la l'algorithme par les valeurs du tableau. On teste par ainsi plusieurs
   * tableaux jusqu'a trouver le résultat de l'algorithme (Ex: 66).
   *
   * @param resultat, le resultat qu'on souhaite calculé.
   */
  public void lancerAlgorithme(int resultat) {
    float[] valeursPossible = new float[9];
    int nombreSolution = 0;

    while (true) {
      //A chaque itération, on regénère le tableau de valeurs possibles pour
      // avoir des valeurs différentes lors de la résolution de l'équation
      valeursPossible = initValeurs(valeursPossible);
      double equationCalcul = valeursPossible[0] + (13 * (valeursPossible[1] / valeursPossible[2])) + valeursPossible[3]
          + 12 * valeursPossible[4] - valeursPossible[5] - 11 + valeursPossible[6] * (valeursPossible[7]
          / valeursPossible[8]) - 10;
      if (equationCalcul == resultat) {
        insertSolution(valeursPossible, resultat);
        nombreSolution++;
      }
      //Le nombre de solution peut être très élévé, on s'arrete à 10.
      if (nombreSolution == 10)
        break;
    }
  }

  /**
   * La solution de l'agorithme est convertie en String et est ensuite insérée
   * dans la table.
   *
   * @param valeursPossible, float array qui contient l'ensemble des solutions de l'algorithme.
   */
  private void insertSolution(float[] valeursPossible, int resultat) {
    StringBuilder stringBuilderSolution = new StringBuilder();
    stringBuilderSolution.append(Math.round(valeursPossible[0]));
    stringBuilderSolution.append("+ (13 * " + "(" + Math.round(valeursPossible[1]) + "/" + valeursPossible[2] + ") + ");
    stringBuilderSolution.append(
        Math.round(valeursPossible[3]) + "+ 12 * " + Math.round(valeursPossible[4]) + "- " + Math
            .round(valeursPossible[5]) + "- 11");
    stringBuilderSolution.append(
        "+" + Math.round(valeursPossible[6]) + "*" + "(" + Math.round(valeursPossible[7]) + "/" + Math
            .round(valeursPossible[8]) + ")" + "" + "- 10");
    ResultatAlgorithme resultatAlgorithme = new ResultatAlgorithme(resultat, stringBuilderSolution.toString());
    setValeursPossibles(valeursPossible, resultatAlgorithme);
    //Si la solution est unique on l'insert en base de données
    if (verifieSolutionUnique(stringBuilderSolution.toString())) {
      resultatAlgorithmeService.creationResultatAlgorithme(resultatAlgorithme);
    }
  }

  /**
   * Affecte les valeurs possibles des inconnues de l'équation du problème.
   *
   * @param valeursPossible,    les valeurs à affecter.
   * @param resultatAlgorithme, le resultat à insérer.
   */
  private void setValeursPossibles(float[] valeursPossible, ResultatAlgorithme resultatAlgorithme) {
    resultatAlgorithme.setInconnue1(Math.round(valeursPossible[0]));
    resultatAlgorithme.setInconnue2(Math.round(valeursPossible[1]));
    resultatAlgorithme.setInconnue3(Math.round(valeursPossible[2]));

    resultatAlgorithme.setInconnue4(Math.round(valeursPossible[3]));
    resultatAlgorithme.setInconnue5(Math.round(valeursPossible[4]));
    resultatAlgorithme.setInconnue6(Math.round(valeursPossible[5]));

    resultatAlgorithme.setInconnue7(Math.round(valeursPossible[6]));
    resultatAlgorithme.setInconnue8(Math.round(valeursPossible[7]));
    resultatAlgorithme.setInconnue9(Math.round(valeursPossible[8]));
  }

  /**
   * On vérifie que la solution à inserer dans la table est unique.
   *
   * @param solution, la solution à verifier dans la table.
   * @return true si la solution est unique, false si non .
   */
  private boolean verifieSolutionUnique(String solution) {
    return resultatAlgorithmeService.verifieSolution(solution) == null;
  }

  /**
   * Initialisation des valeurs possibles pour résoudre l'équation en entrée.
   *
   * @param valeursPossible, array de float
   * @return un array avec l'ensemble de valeurs possibles, le tableau est trié aleartoirement
   */
  private float[] initValeurs(float[] valeursPossible) {
    List<Float> a = new ArrayList<>();
    //On crée une liste de float avec 9 valeurs de 1 à 9
    for (float i = 1; i <= valeursPossible.length; i++) { //to generate from 0-10 inclusive.
      a.add(i);
    }
    //Permutation àleatoire de notre liste de float
    Collections.shuffle(a);
    //On retourne la liste convertie en tableau de float.
    return ArrayUtils.toPrimitive(a.toArray(new Float[0]), 0.0F);

  }

}
