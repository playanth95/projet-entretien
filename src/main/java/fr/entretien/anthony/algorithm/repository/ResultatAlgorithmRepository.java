package fr.entretien.anthony.algorithm.repository;

import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Classe repository pour le résultat de l'algorithme.
 *
 * @author aattia
 */
@Repository
public interface ResultatAlgorithmRepository extends CrudRepository<ResultatAlgorithme, Long> {

  /**
   *
   * @param resultat, le résultat de l'algorithme ( Ex: 55, 66).
   * @return la liste des solutions et informations liées au réultat de l'algorithme.
   */
  List<ResultatAlgorithme> findAllByResultat(int resultat);

  /**
   * @param resultat, le résultat à modifier.
   * @return l'entité resultat algorithme.
   */
  ResultatAlgorithme findByResultat(int resultat);

  /**
   * Supprime toutes les lignes de la table
   * resultat_algorithme en fonction du résultat en paramètre
   *
   * @param resultat, le résultat à  supprimer.
   */
  Long deleteByResultat(int resultat);

  /**
   * On vérifie que la solution de l'algorithme à inserer dans la table est unique.
   *
   * @param solution, la solution à vérifier.
   * @return null si la solution n'existe pas, le resultat algorithme si oui .
   */
  ResultatAlgorithme findBySolution(String solution);

}
