package fr.entretien.anthony.algorithm.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

/**
 * Cette classe de controlleur permet d'intercepter les exceptions de hautniveau de type
 * SqlException,NullPointerException qui peuvent se produire au sein de certains traitements.
 * Si une de ces exceptions est levée, un des controlleur qui intercepte cette exception
 * renverra un message.
 *
 * @author aattia
 */

@RestControllerAdvice
public class RestControllerAdviceErrors {

    /**
     * Instance du logger.
     */
    private Logger loggerRestControllerAdviceException = LoggerFactory
        .getLogger(RestControllerAdviceErrors.class);

    /**
     * Retourne les exceptions sql levées.
     *
     * @param exception, l'exception levée, ici une exception SQL.
     * @return l'exception sql détaillée levée.
     */
    @ExceptionHandler(SQLException.class)
    public ResponseEntity sqlExceptionHandler(
        SQLException exception) {
        return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
