package fr.entretien.anthony.algorithm.rest.controller;

import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import fr.entretien.anthony.algorithm.repository.ResultatAlgorithmRepository;
import fr.entretien.anthony.algorithm.service.ResultatAlgorithmeService;
import fr.entretien.anthony.algorithm.traitement.TraitementAlgorithme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

/**
 * Classe de controlleur pour les resultats de l'algorithme
 *
 * Gestion de l'ensemble des endpoints liées aux résultats de l'algorithme.
 *
 * @author aattia
 */
@CrossOrigin(allowCredentials = "true", allowedHeaders = "*", origins = "*")
@RequestMapping("/api")
@RestController
public class ResultatAlgorithmeController {

    private Logger logger = LoggerFactory.getLogger(ResultatAlgorithmeController.class);

    @Autowired
    ResultatAlgorithmRepository resultatAlgorithmRepository;

    @Autowired
    ResultatAlgorithmeService resultatAlgorithmeService;

    @Autowired
    TraitementAlgorithme traitementAlgorithme;

    /**
     * GET "/resultats/{resultat}", Endpoint de récupération des résultats de l'algorithme.
     *
     * @autho aattia
     * @return les résultats de l'algorithme.
     */
    @GetMapping(value = "/resultats/{resultat}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ResultatAlgorithme>> recupererResultats(@PathVariable int resultat) {

      Instant start = Instant.now();
      traitementAlgorithme.lancerAlgorithme(resultat);
      Instant end = Instant.now();
      logger.info("Durée d'éxécution de l'algorithme : " + Duration.between(start, end).getNano());

      return ResponseEntity.ok().body(resultatAlgorithmeService.recupererResultatsAlgorithme(resultat));
    }

    /**
     * PUT "/resultats/{resultat}", Endpoint de récupération des résultats de l'algorithme.
     *
     * @autho aattia
     * @return les résultats de l'algorithme.
     */
    @PutMapping(value = "/resultats/{resultat}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ResultatAlgorithme>> modifierResultatAlgorithme(@PathVariable int resultat) {
        resultatAlgorithmeService.modifierResultatAlgorithme(resultat);
        return ResponseEntity.ok().body(resultatAlgorithmeService.recupererResultatsAlgorithme(resultat));
    }

    /**
     * DELETE "/resultats/", Endpoint de suppression d'un résultat de l'algorithme.
     *
     * @autho aattia
     * @return les résultats de l'algorithme.
     */
    @DeleteMapping(value = "/resultats/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ResultatAlgorithme>> supprimerResultatAlgorithme(@RequestBody ResultatAlgorithme
        resultatAlgorithme) {
        resultatAlgorithmeService.supprimerResultatAlgorimthe(resultatAlgorithme);
        return ResponseEntity.ok().body(resultatAlgorithmRepository.findAllByResultat(resultatAlgorithme.getResultat
            ()));
    }
}
