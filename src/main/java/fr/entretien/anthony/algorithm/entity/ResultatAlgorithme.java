package fr.entretien.anthony.algorithm.entity;

import javax.persistence.*;

/**
 * Entity faisant le lien avec la table resultat_algorithme
 *
 *
 *
 * @author aattia
 */
@Entity
@Table(name = "resultat_algorithme")
public class ResultatAlgorithme {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  /**
   * String représentant la solution du résultat
   * Ex: "valeursCase[0]=1  * 13 + valeursCase[1] = 2"
   */
  private String solution;
  /**
   *
   * Résultat de l'équation
   */
  private int resultat;


  private int inconnue1;

  private int inconnue2;

  private int inconnue3;

  private int inconnue4;

  private int inconnue5;

  private int inconnue6;

  private int inconnue7;

  private int inconnue8;

  private int inconnue9;


  /**
   * Constructeur vide
   */
  public ResultatAlgorithme() {
  }

  /**
   * Constructeur
   *
   * @param resultat, le resultat de l'algorithme.
   * @param solution, une solution possible à l'algorithme.
   */
  public ResultatAlgorithme(int resultat,String solution) {
    this.resultat = resultat;
    this.solution = solution;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSolution() {
    return solution;
  }

  public void setSolution(String solution) {
    this.solution = solution;
  }

  public int getResultat() {
    return resultat;
  }

  public void setResultat(int resultat) {
    this.resultat = resultat;
  }

  public int getInconnue1() {
    return inconnue1;
  }

  public void setInconnue1(int inconnue1) {
    this.inconnue1 = inconnue1;
  }

  public int getInconnue2() {
    return inconnue2;
  }

  public void setInconnue2(int inconnue2) {
    this.inconnue2 = inconnue2;
  }

  public int getInconnue3() {
    return inconnue3;
  }

  public void setInconnue3(int inconnue3) {
    this.inconnue3 = inconnue3;
  }

  public int getInconnue4() {
    return inconnue4;
  }

  public void setInconnue4(int inconnue4) {
    this.inconnue4 = inconnue4;
  }

  public int getInconnue5() {
    return inconnue5;
  }

  public void setInconnue5(int inconnue5) {
    this.inconnue5 = inconnue5;
  }

  public int getInconnue6() {
    return inconnue6;
  }

  public void setInconnue6(int inconnue6) {
    this.inconnue6 = inconnue6;
  }

  public int getInconnue7() {
    return inconnue7;
  }

  public void setInconnue7(int inconnue7) {
    this.inconnue7 = inconnue7;
  }

  public int getInconnue8() {
    return inconnue8;
  }

  public void setInconnue8(int inconnue8) {
    this.inconnue8 = inconnue8;
  }

  public int getInconnue9() {
    return inconnue9;
  }

  public void setInconnue9(int inconnue9) {
    this.inconnue9 = inconnue9;
  }
}
