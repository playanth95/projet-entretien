package fr.entretien.anthony.algorithm.service;

import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe de service pour les résultats de l'algorithme
 * Appel du repository et des requêtes JPA.
 *
 * @author aattia.
 */
@Service
public interface ResultatAlgorithmeService {

  /**
   * Retourne les résultats de l'algorithme
   *
   * @Return, liste des résultats de l'algorithme.
   */
  List<ResultatAlgorithme> recupererResultatsAlgorithme(int resultat);

  /**
   * Modifie le résultat de l'algortihme
   *
   * @param resultat, le resultat à modifier.
   */
  void modifierResultatAlgorithme(int resultat);

  /**
   * Supprime le résultat de l'algortihme
   *
   * @param resultat, le resultat à modifier.
   */
  void supprimerResultatAlgorimthe(ResultatAlgorithme resultat);

  /**
   * Crée une nouvelle ligne dans la table resultat_algorithme
   *
   * @param resultatAlgorithme,
   * @return la ligne crée en table
   */
  ResultatAlgorithme creationResultatAlgorithme(ResultatAlgorithme resultatAlgorithme);

  /**
   * On vérifie que la solution de l'algorithme à inserer dans la table est unique.
   * @param solution, la solution à vérifier.
   * @return null si la solution n'existe pas, le resultat algorithme si oui .
   */
  ResultatAlgorithme verifieSolution(String solution);
}
