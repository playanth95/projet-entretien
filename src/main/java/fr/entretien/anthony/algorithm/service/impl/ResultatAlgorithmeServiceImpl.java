package fr.entretien.anthony.algorithm.service.impl;

import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import fr.entretien.anthony.algorithm.repository.ResultatAlgorithmRepository;
import fr.entretien.anthony.algorithm.service.ResultatAlgorithmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implémentation de l'interface du service ResultatAlgorithmeService
 *
 * Created by aattia on 14/07/2018.
 */
@Service
@Transactional
public class ResultatAlgorithmeServiceImpl implements ResultatAlgorithmeService {

  @Autowired
  private ResultatAlgorithmRepository resultatAlgorithmeRepository;

  @Override
  public List<ResultatAlgorithme> recupererResultatsAlgorithme(int resultat) {
    return resultatAlgorithmeRepository.findAllByResultat(resultat);
  }

  @Override
  public void modifierResultatAlgorithme(int resultat) {
    ResultatAlgorithme resultatAlgorithme = resultatAlgorithmeRepository.findByResultat(resultat);
    resultatAlgorithmeRepository.save(resultatAlgorithme);
  }

  @Override
  public void supprimerResultatAlgorimthe(ResultatAlgorithme resultat) {
    resultatAlgorithmeRepository.delete(resultat);
  }

  @Override
  public ResultatAlgorithme creationResultatAlgorithme(ResultatAlgorithme resultatAlgorithme) {
    return resultatAlgorithmeRepository.save(resultatAlgorithme);
  }

  @Override
  public ResultatAlgorithme verifieSolution(String solution) {
    return null;
  }
}
