package fr.entretien.anthony.algorithm.traitement;

import fr.entretien.anthony.algorithm.Application;
import fr.entretien.anthony.algorithm.service.ResultatAlgorithmeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * Created by aattia on 15/07/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations = "classpath:tapplication.properties")
public class TraitementAlgorithmeTest {
  private TraitementAlgorithme traitementAlgorithme;

  @Mock
  ResultatAlgorithmeService resultatAlgorithmeService;

  @Before
  public void initTest(){
    traitementAlgorithme = new TraitementAlgorithme();
  }

  @Test
  public void lancerAlgorithmeTest() throws Exception {
    traitementAlgorithme.lancerAlgorithme(66);
    Assert.assertEquals(10,
        resultatAlgorithmeService.recupererResultatsAlgorithme(66).size() );

  }

  @Test
  public void lancerAlgorithme() throws Exception {

  }

}