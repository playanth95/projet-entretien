package fr.entretien.anthony.algorithm.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.entretien.anthony.algorithm.Application;
import fr.entretien.anthony.algorithm.entity.ResultatAlgorithme;
import fr.entretien.anthony.algorithm.service.ResultatAlgorithmeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aattia on 12/07/2018.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations = "classpath:tapplication.properties")
public class ResultatTraitementAlgorithmeControllerTest {

  private Logger logger = LoggerFactory.getLogger(ResultatTraitementAlgorithmeControllerTest.class);
  /**
   * Injection de dependences.
   */
  @Autowired
  private  ResultatAlgorithmeController algorithmeController;

  @Autowired
  private ResultatAlgorithmeService resultatAlgorithmRepository;
  /**
   * Point d'entrée de test du spring mvc.
   */
  private MockMvc mockMvc;

  @Before
  public void init() {
    //Initialise une nouvelle instance MockMvc avec en paramètre une instance du controller à
    // tester
    this.mockMvc = MockMvcBuilders.standaloneSetup(algorithmeController).build();
  }

  /**
   * Test du endpoint "/api/resultats"
   *
   * @author aattia
   */
  @Test
  public void test_EndPoint_Resultats_Algo_Retourne_Ok() throws Exception {
    //Appel du endpoint de création de partie
    final MvcResult mvcResult = this.mockMvc.perform(get("/api" + "/" + "resultats" + "/66"))
        //Vérifie que le statut  http  retourné par le endpoint est ok.
        .andExpect(status().isOk()).andExpect(jsonPath("$.[*].resultat", hasItem(66))).andReturn();
    List<ResultatAlgorithme> resultatAlgorithmes = resultatAlgorithmRepository.recupererResultatsAlgorithme(66);
    Assert.assertEquals(10, resultatAlgorithmes.size());

    logger.info(mvcResult.toString());
  }

  /**
   * Test du endpoint "/api/resultats"
   *
   * @author aattia
   *
   *
   */
  @Test
  public void test_EndPoint_Resultats_Algo_Supprime_Resultat_Ok() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    ResultatAlgorithme resultatAlgorithme = new ResultatAlgorithme(100, "solution 1");

    resultatAlgorithmRepository.creationResultatAlgorithme(resultatAlgorithme);

    //Conversion de l'objet à supprimer en json.
    String resultatJson = mapper.writeValueAsString(resultatAlgorithme);

    //Appel du endpoint de création de partie
    final MvcResult mvcResult = this.mockMvc
        .perform(delete("/api" + "/resultats/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(resultatJson))
        //Vérifie que le statut  http  retourné par le endpoint est ok.
        .andExpect(status().isOk()).andReturn();

    //On vérifie que le resultat de l algorithme est bien supprimé en base
    List<ResultatAlgorithme> resultatAlgorithmes = resultatAlgorithmRepository.
        recupererResultatsAlgorithme(100);
    Assert.assertNull(resultatAlgorithme);
  }


}